﻿import QtQuick 2.3

Rectangle {
  id: rectangle2
  width: 360
  height: 360

  MouseArea {
    id: ma
    anchors.fill: parent
    onClicked: {
    }

    Rectangle {
        id: rectangle1
        x: 32
        y: 36
        width: 235
        height: 102
        color: "#9d1010"
        radius: 21
        border.width: 12
        border.color: "#0b047f"
    }
  }

  Text {
    anchors.centerIn: parent
    text: "Hello World"
  }
  states: [
    State {
      name: "another"
      when: ma.pressed

      PropertyChanges {
        target: rectangle1
        color: "#42a603"
        transformOrigin: Item.Center
        scale: 0.7
        rotation: 14
      }

//      PropertyChanges {
//        target: rectangle2
//        rotation: 0
//          }
    }
  ]

  transitions: [
    Transition {
      to: "another"
      ParallelAnimation {
        NumberAnimation { targets: rectangle1; properties: "rotation, scale"; duration: 1000; easing.type: Easing.InOutQuad }
        ColorAnimation { target: rectangle1 ; property: "color"; duration: 1000; easing.type: Easing.InQuint }
      }
    },
    Transition {
      from: "another"
      ParallelAnimation {
        NumberAnimation { targets: rectangle1; properties: "rotation, scale"; duration: 1000; easing.type: Easing.InOutQuad }
        ColorAnimation { target: rectangle1 ; property: "color"; duration: 1000; easing.type: Easing.InQuint }
      }
    }

  ]
}

