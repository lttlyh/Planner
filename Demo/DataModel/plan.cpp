#include "plan.h"

Plan::Plan(QObject *parent) :
  QObject(parent)
{
}

Plan::Plan(const Plan &p, QObject *parent) :
  QObject(parent),
  m_title(p.title()),
  m_date(p.date()),
  m_description(p.description()),
  m_state(p.state())
{
}

QString Plan::title() const
{
  return m_title;
}

void Plan::setTitle(const QString &title)
{
  if(title == m_title)
    return;
  m_title = title;
  emit titleChanged();
}

QDateTime Plan::date() const
{
  return m_date;
}

void Plan::setDate(const QDateTime &date)
{
  if(date == m_date)
    return;
  m_date = date;
  emit dateChanged();
}

QString Plan::description() const
{
  return m_description;
}

void Plan::setDescription(const QString &desc)
{
  if(desc == m_description)
    return;
  m_description = desc;
  emit descriptionChanged();
}

Plan::PLAN_STATE Plan::state() const
{
  return m_state;
}

void Plan::setState(int s)
{
  if(s == m_state)
    return;
  m_state = (Plan::PLAN_STATE)s;
  emit stateChanged();
}
