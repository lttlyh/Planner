#ifndef PLANNERMODEL_H
#define PLANNERMODEL_H

#include <QAbstractListModel>
#include "plan.h"

class PlannerModel : public QAbstractListModel
{
  Q_OBJECT
public:
  enum PLANNER_ROLE {
    TITLE = Qt::UserRole + 1,
    DATE,
    DESCRIPTION,
    STATE
  };

  static const char *TITLE_STR;
  static const char *DATE_STR;
  static const char *DESCRIPTION_STR;
  static const char *STATE_STR;

  explicit PlannerModel(QObject *parent = 0);
  virtual int rowCount(const QModelIndex &parent) const;
  virtual QVariant data(const QModelIndex &index, int role) const;
  virtual Qt::ItemFlags flags(const QModelIndex &index) const;
  virtual QHash<int, QByteArray> roleNames() const;
  virtual bool insertRows(int row, int count, const QModelIndex &parent);
  virtual bool removeRows(int row, int count, const QModelIndex &parent);

  Q_INVOKABLE void prepend(Plan *p);
  Q_INVOKABLE void remove(int i);

signals:

public slots:

private:
  static QList<Plan *> m_plans;
  static QObject gparent;
};

#endif // PLANNERMODEL_H
