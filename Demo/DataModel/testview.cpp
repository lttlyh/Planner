#include "testview.h"
#include "plannermodel.h"

#include <QAbstractListModel>

testView::testView(QWidget *parent) :
  QListView(parent)
{
  PlannerModel *model = new PlannerModel();
  this->setModel(model);
  connect(model, SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)),
          this, SLOT(dataChanged(QModelIndex,QModelIndex,QVector<int>)));
  connect(model, SIGNAL(rowsInserted(QModelIndex, int, int)),
          this, SLOT(rowsInserted(QModelIndex,int,int)));
  connect(model, SIGNAL(rowsAboutToBeRemoved(QModelIndex, int, int)),
          this, SLOT(rowsAboutToBeRemoved(QModelIndex,int,int)));
}
