import QtQuick 2.3
import QtQuick.Window 2.2
import Planner 0.1

Window {
  visible: true
  width: 400
  height: 300

  PlannerModel {
    id: model
  }

  Component {
    id: delegate
    Rectangle {
      x: 10
      width: ListView.view.width - 40
      height: 40
      color: 'green'
      Text {
        anchors.centerIn: parent
        text: model.title
      }
    }
  }

  ListView {
    model: model
    delegate: delegate
    anchors.fill: parent
    spacing: 10
  }

  Rectangle {
    id: addBtn
    /*
      anchors.margins: 20
      anchors.left: parent.left
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      */
    x: 20
    y: parent.height - height - 10
    width: parent.width / 2 - 30
    height: 80
    color: 'lightsteelblue'

    Component {
      id: plan
      Plan {}
    }

    MouseArea {
      anchors.fill: parent
      onClicked: {
        var p = plan.createObject(this, {title: "test title"});
        model.prepend(p);
      }
    }
  }

  Rectangle {
    id: delBtn
    x: parent.width / 2 + 20
    y: parent.height - height - 10
    width: parent.width / 2 - 30
    height: 80
    color: 'gray'

    MouseArea {
      anchors.fill: parent
      onClicked: {
        model.remove(3);
      }
    }
  }
}
