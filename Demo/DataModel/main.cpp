#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>

#include "plannermodel.h"
#include "testview.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<Plan>("Planner", 0, 1, "Plan");
    qmlRegisterType<PlannerModel>("Planner", 0, 1, "PlannerModel");
    // mock up
    PlannerModel model;
    Plan p;
    p.setTitle("Title 1");
    p.setDate(QDateTime::currentDateTime());
    p.setDescription("experimental implementation");
    p.setState(Plan::NOT_START);
    model.prepend(new Plan(p));
    p.setTitle("Title 2");
    model.prepend(new Plan(p));
    p.setTitle("Title 3");
    model.prepend(new Plan(p));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    //testView v;
    //v.show();

    return app.exec();
}
