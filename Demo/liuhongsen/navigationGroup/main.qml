import QtQuick 2.3
import QtQuick.Window 2.2

Window {
    id: window1
    visible:true
   opacity: 1
    minimumWidth: 250
    maximumWidth: 250
    maximumHeight: 400
    minimumHeight: 400
    title:"Planner"
    width: 250
    height: 400



    property int  n: 1
    property int cnt:0

    Rectangle {
        height:45
        id: toolbar
        color: "#d1e05b"
        radius: 12
        opacity:1
        rotation: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0


        Behavior on opacity{NumberAnimation{duration:500}}
        Rectangle {
            id: rectangle1
            x: 0
            y: 44
            width: 250
            height: 355
            color: "#8091eb6f"
            smooth: true

            anchors.bottom:window1.bottom

            Text {
                id: textwelcome
                x: 79
                y: 45
                anchors.horizontalCenter: parent.horizontalCenter
                color: "#4295d4"
                text: qsTr("欢迎使用")
                font.strikeout: false
                style: Text.Normal
                font.family: "Courier"
                font.bold: true
                font.pixelSize: 23
                opacity: 1

                Behavior on opacity {NumberAnimation{duration:1000}}
            }
        }
    }


        Image {
            id: addfile
            x: 197
            y: 8
            width: 25
            height: 25
            source: "file_add.png"
            anchors.right: parent.right
            anchors.rightMargin: 28

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    //cnt++;taskin();
                   tasknode.insert(0,{"content":"fdsfe"})
                }
            }
        }


//**********图象显示**********
        Image {
            id: groupphoto
            x: 8
            y: 12
            width: 27
            height: 25
            fillMode: Image.Stretch
            source: "user_manage.png"

        }

        Image {
            id: robot
            x: 220
            y: 370
            width: 30
            height: 30
            source: "Users Program Group.png"

        }



        Image {
            id: kittyright
            x: 127
            y: 132
            width: 104
            height: 162
            source: "20110228143052110.png"


            Behavior on opacity {NumberAnimation{duration:500}}
        }

        Image {
            id: kittyleft
            x: 24
            y: 132
            width: 104
            height: 162
            source: "20110228143052102.png"


            Behavior on opacity {NumberAnimation{duration:500}}
        }
//**************************

//********开始按钮**********
    Rectangle {
        id: recbegin
        x: 88
        y: 306
        width: 75
        height: 30
        color: "#5aea23"
        radius: 15
        border.width: 3
        Text{
            text:'Begin'
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.bold:true
            font.pixelSize:recbegin.width/4
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                kittyright.opacity=0;
                kittyleft.opacity=0;
                textwelcome.opacity=0;
                recbegin.opacity=0;
            }

        }
    }
//**************

//************添加任务条*******

    property int cnter:0

    ListView {
        y:45
        x:0
        id:tasklist
        model:tasknode
        opacity: 1
        delegate: Rectangle{
            width:window1.width
            //opacity: 0.1
            height:50
            //color: 'purple'
            radius:8

            Text {
                 anchors.centerIn: parent
                 text: content
               }


            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {delet.opacity=0.8;delet.enabled=true}
                onExited: {delet.opacity=0;delet.enabled=false}
                onClicked: {text.x=10;cnter=index;
                    tasklist.opacity=0.2;
                    toolbar.opacity=0.2;
                    textIn.text=""
                    }
            }


            Image {
                id: delet
                x: 215
                y: 15
                width: 20
                height: 20
                anchors{
                    right:parent.right
                    rightMargin: 15
                }

                fillMode: Image.Stretch
                source: "delete.png"
                opacity:0
                enabled:false

                MouseArea{
                    anchors.fill:delet
                    onClicked: tasknode.remove(index,1)
                }
           }


        }

        Behavior on opacity{NumberAnimation{duration:500}}

        add: Transition {
                NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 400 }
                NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 400 }
            }

        displaced : Transition {
                NumberAnimation { properties: "x,y"; duration: 600; easing.type: Easing.OutBounce }
            }
        remove: Transition {
                 ParallelAnimation {
                      NumberAnimation { property: "opacity"; to: 0; duration: 500 }
                      NumberAnimation { properties: "x,y"; to:100;duration: 400}
                   }
               }

            //focus: true
            //Keys.onSpacePressed: model.insert(0, { "name": "Item " + model.count })

            //highlight:Rectangle{color:'yellow';radius:8;height: 80}
            //spacing: 20

    }
    Rectangle{
        id:text
        x:250
        y:70
        width:230
        opacity:0.8
        //height:widow1.height-toolbar.height-20
        height:305
        radius:10
        color:"#043434"
        TextEdit {
            id:textIn
            visible:true
            focus: true
            selectByMouse: true
            //enabled: true

            anchors.fill: parent
            wrapMode: TextInput.WrapAnywhere
            //anchors.top: parent.top
            anchors.topMargin: 15
            anchors.rightMargin: 8
            anchors.leftMargin: 8
            anchors.bottomMargin: 8

            font.bold: true
            //font.pixelSize: 15
            //font.letterSpacing: 2
            color:'yellow'
        }
        Behavior on x{
            NumberAnimation{duration:700}
        }
        Image {
            id: ok
            width:25
            height:25
            source: "ok.png"
            fillMode:Image.Stretch
            anchors{
                top:parent.top
                topMargin: 15
                right:parent.right
                rightMargin: 15
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    tasknode.setProperty(cnter,"content",textIn.text);
                    text.x=250;
                    tasklist.opacity=1;
                    toolbar.opacity=1;

                    //tasknode.insert(0,{"content":"fdsfe"})
                }
            }
        }
    }


    ListModel{
        id:tasknode

    }

}
