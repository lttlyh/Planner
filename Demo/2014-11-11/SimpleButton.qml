import QtQuick 2.0

Rectangle {
  id: root
  width: 60
  height: 35

  property string text: 'Button'
  property color normalColor: 'green'
  property color hoverColor: Qt.lighter(normalColor)
  property color activeColor: Qt.darker(normalColor)
  color: ma.pressed ? activeColor : (ma.containsMouse ? hoverColor : normalColor)
  signal clicked

  radius: 10
  border.color: Qt.darker(normalColor)
  border.width: 2

  Text {
    anchors.centerIn: parent
    text: root.text
  }

  MouseArea {
    id: ma
    hoverEnabled: true
    anchors.fill: parent
    onClicked: root.clicked();
  }

  Behavior on color {
    ColorAnimation { duration: 70 }
  }
}
