import QtQuick 2.0
import QtQuick.Controls 1.2

Rectangle {
  id: root
  width: 400
  height: 300
  color: 'transparent'

  Rectangle {
    id: part
    anchors.fill: parent
    anchors.margins: 10
    anchors.bottomMargin: 60
    color: '#ddaa22'

    Text {
      anchors.centerIn: parent
      font.pixelSize: parent.height / 2
      font.bold: true
      text: "Hello"
    }
  }

  Rectangle {
    id: layer
    width: parent.width
    height: parent.height
    color: "#ffffff"
    opacity: 0
  }

  Rectangle {
    id: dialog
    width: parent.width - 40
    height: parent.height - 100
    x: parent.width + 20
    y: 20
    color: '#6363bb'
    Rectangle {
      width: parent.width / 2
      height: 40
      anchors.left: parent.left
      anchors.bottom: parent.bottom
      color: 'red'
      Text {
        text: 'Back'
        anchors.centerIn: parent
      }
      MouseArea {
        anchors.fill: parent
        onClicked: root.state = ''
      }
    }

    Rectangle {
      width: parent.width / 2
      height: 40
      anchors.right: parent.right
      anchors.bottom: parent.bottom
      color: 'green'
      Text {
        text: 'Confirm'
        anchors.centerIn: parent
      }
      MouseArea {
        anchors.fill: parent
        onClicked: root.state = ''
      }
    }
  }

  SimpleButton {
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    anchors.margins: 10
    width: parent.width - 20
    height: 40
    text: 'Change'
    onClicked: root.state = 'newState'
  }

  states: [
    State {
      name: "newState"
      /*PropertyChanges {
        target: part
        color: 'green'
      }*/
      PropertyChanges {
        target: layer
        opacity: 0.7
      }
      PropertyChanges {
        target: dialog
        x: 20
      }
    }
  ]

  transitions: [
    Transition {
      to: 'newState'
      reversible: true
      animations: [
        ColorAnimation { target: part; duration: 200 },
        SequentialAnimation {
          NumberAnimation { target: layer; property: 'opacity'; duration: 200 }
          NumberAnimation { target: dialog; property: 'x'; duration: 300; easing.type: Easing.InOutCubic; }
        }
      ]
    }
  ]
}
