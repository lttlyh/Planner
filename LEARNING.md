#学习记录

##目标

* 了解QML语言的基本知识
* 了解使用QtQuick的开发模式，常用的QtQuick组件，基本的动画效果，界面布局
* 了解将C++类包装成为QML组件的方法
* ...

##记录

**2014-11-05**

初次演示了QML的基本知识和QtQuick，希望大家回去后多多学习。

老师发的示例大家可以在Qt的安装目录下的Examples目录下找到，与qml和qtquick相关的目录如下：

* declarative
* qml
* quick