.pragma library

function makeDatestringFromeDate(d) {
  return makeDatestringFromParsedInfo({
    year: d.getFullYear(),
    month: d.getMonth() + 1,
    day: d.getDate(),
    hours: d.getHours(),
    minutes: d.getMinutes(),
    seconds: d.getSeconds()
  });
}

function makeDatestringFromParsedInfo(obj) {
  var arr = [
        truncateLeft(obj.year, 4),
        "-",
        truncateLeft(obj.month, 2),
        "-",
        truncateLeft(obj.day, 2),
        "T",
        truncateLeft(obj.hours, 2),
        ":",
        truncateLeft(obj.minutes, 2),
        ":",
        truncateLeft(obj.seconds, 2)
      ];
  return arr.join("");
}

function refineDatestring(dateString) {
  var parsedInfo = parseDatestring(dateString);
  var refined = makeDatestringFromParsedInfo(parsedInfo);
  return refined;
}

var re = /(\d{4})-(\d\d?)-(\d\d?)[ T](\d\d?):(\d\d?):(\d\d?)/;
function parseDatestring(dateString) {
  var mc = re.exec(dateString);
  return {
    year: mc[1],
    month: mc[2],
    day: mc[3],
    hours: mc[4],
    minutes: mc[5],
    seconds: mc[6]
  };
}

function makeDateFromDatestring(dateString) {
  var info = parseDatestring(dateString);
  var date = new Date();
  date.setFullYear(info.year);
  date.setMonth(info.month - 1);
  date.setDate(info.day);
  date.setHours(info.hours);
  date.setMinutes(info.minutes);
  date.setSeconds(info.seconds);
  return date;
}

function truncateLeft(str, len, padchar) {
  str = str.toString();
  if(typeof padchar === 'undefined')
    padchar = '0';
  if(str.length === len)
    return str;
  else if(str.length < len) {
    var arr = [];
    var i = len - str.length
    while(i--) {
      arr.push(padchar);
    }
    arr.push(str);
    return arr.join("");
  }else{
    return str.slice(str.length - len);
  }
}

// PlannerModel role to value map, construct it in MainView.qml

var map = {}
