.pragma library

var pallete = {};
pallete.background = "#334569"//#525252";
//pallete.background = "#0066FF"; // blue 1
//pallete.background = "#ffffff"; //"#0099FF";
pallete.foreground = "#eaeaea";
pallete.control_color = "#53668c"//666666"
//pallete.welcome_back = "#8091eb";
pallete.background_gradient_top = "#7e8fb2";
pallete.background_gradient_bottom = "#334569";
pallete.layer_color = "#ffffff";
// bootstrap color style
pallete.primary = "#428bca";
pallete.success = "#5cb85c";
pallete.info = "#5bc0de";
pallete.warning = "#f0ad4e";
pallete.danger = "#d9534f";

pallete.critical = pallete.danger;//"#dd3333";
pallete.dialog_background = "#53668c"//8996af"; // "#0066FF";
pallete.dialogInputBackground = "#334569"//0044aa";

var window = {};
window.width = 300;
window.height = 400;

var toolbar = {};
toolbar.height = 60;
toolbar.spacing = 10;

var font_size = {};
font_size.small = 10
font_size.normal = 14;
font_size.middle = 20;
font_size.large = 24;
font_size.xlarge = 28;

var planviewitem = {};
planviewitem.height = 80;
planviewitem.sidebarwidth = 90;

var time = {};
time.normal = 200;
time.quick = 100;
time.slow = 300;

var dialog  = {};
dialog.margin_top = window.height / 10;
dialog.margin_bottom = window.height / 10;
dialog.margin_left = window.width / 8;
dialog.margin_right = window.width / 8;
dialog.height = window.height - dialog.margin_top - dialog.margin_bottom;
dialog.width = window.width - dialog.margin_left - dialog.margin_right;
dialog.btnGrpHeight = dialog.height / 8;
dialog.titleInputHeight = dialog.height / 8;
dialog.dateInputHeight = dialog.height / 8;
dialog.descriptionInputHeight = dialog.height * 5 / 8

var iconbutton = {};
iconbutton.imgwidth = 40;
iconbutton.imgheight = 40;
