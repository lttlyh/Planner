#ifndef PERSISTENTSTORAGE_H
#define PERSISTENTSTORAGE_H

#include "plan.h"
#include <QVariant>
#include <QSqlQuery>

class PersistentStorage
{
public:
  PersistentStorage();
  static void connect();
  static Plan &addPlan(Plan &p);
  static void removePlan(Plan &p);
  static void updatePlan(Plan &p);
  static QList<Plan *> findAll();

private:
  static QSqlError execAddPlan(QSqlQuery &q, Plan &p, bool &hasError);
  static QSqlError execUpdatePlan(QSqlQuery &q, Plan &p, bool &hasError);
  static QSqlError execRemovePlan(QSqlQuery &q, Plan &p, bool &hasError);
  static QSqlError execFindAll(QSqlQuery &q, bool &hasError);
  static QString connectionString();
};

#endif // PERSISTENTSTORAGE_H
