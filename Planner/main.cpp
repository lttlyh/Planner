#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>

#include "plannermodel.h"
#include "persistentstorage.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<Plan>("Planner", 0, 1, "Plan");
    qmlRegisterType<PlannerModel>("Planner", 0, 1, "PlannerModel");

    // data initialization
    PersistentStorage::connect();
    PlannerModel model;
    model.import(PersistentStorage::findAll());


    engine.load(QUrl(QStringLiteral("qrc:/controls/window.qml")));

    return app.exec();
}

