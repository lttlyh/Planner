#ifndef PLAN_H
#define PLAN_H

#include <QObject>
#include <QDateTime>

class Plan : public QObject
{
  Q_OBJECT
  Q_ENUMS(PLAN_STATE)
  Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
  Q_PROPERTY(QDateTime date READ date WRITE setDate NOTIFY dateChanged)
  Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
  Q_PROPERTY(PLAN_STATE state READ state WRITE setState NOTIFY stateChanged)
public:
  enum PLAN_STATE {
    NOT_START,
    PROGRESSING,
    FINISHED
  };

  static const char *NOT_START_STR;
  static const char *PROGRESSING_STR;
  static const char *FINISHED_STR;

  explicit Plan(QObject *parent = 0);
  Plan(const Plan &p, QObject *parent = 0);
  QString title() const;
  void setTitle(const QString &title);
  QDateTime date() const;
  void setDate(const QDateTime &date);
  QString description() const;
  void setDescription(const QString &desc);
  PLAN_STATE state() const;
  void setState(int s);

  // id of the entry in the database
  int id() const;
  void setId(int i);

  // comparer
  bool operator <(const Plan &another) const;

signals:
  void titleChanged();
  void dateChanged();
  void descriptionChanged();
  void stateChanged();

public slots:

private:
  QString m_title;
  QDateTime m_date;
  QString m_description;
  PLAN_STATE m_state;
  int m_id;
};

#endif // PLAN_H
