import QtQuick 2.3

import '../js/statics.js' as Statics

Rectangle {
  id: root
  //color: Statics.pallete.background
  gradient: Gradient {
    GradientStop { color: Statics.pallete.background_gradient_top; position: 0}
    GradientStop { color: Statics.pallete.background_gradient_bottom; position: 1}
  }

  /* welcome and main view */
  Row {
    id: row
    Welcome {
      id: welcome
      width: root.width
      height: root.height
      onEnter: root.state = 'main'
    }
    MainView {
      id: mainview
      width: root.width
      height: root.height
    }
  }

  /* states and transitions */
  states: [
    State {
      name: "main"
      PropertyChanges {
        target: row
        x: - root.width
      }
    }
  ]

  transitions: [
    Transition {
      from: ""
      to: "main"
      animations: [
        NumberAnimation { target: row; property: "x"; duration: Statics.time.normal; easing.type: Easing.InOutCubic }
      ]
    }
  ]
}
