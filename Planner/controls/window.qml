import QtQuick 2.3
import QtQuick.Window 2.2
import '../js/statics.js' as Statics

Window {
  id: root
  visible:true
  // hide window border
  //flags: Qt.FramelessWindowHint
  title:"Planner"
  minimumWidth: Statics.window.width
//  maximumWidth: Statics.window.width
  width: Statics.window.width
//  maximumHeight: Statics.window.height
  minimumHeight: Statics.window.height
  height: Statics.window.height


  Main {
    width: root.width
    height: root.height
  }
}
