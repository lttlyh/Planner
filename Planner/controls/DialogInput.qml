import QtQuick 2.0

import '../js/statics.js' as Statics

Item {
  id: root
  width: 200
  height: 60

  property string label
  property alias text: input.text
  property alias inputMask: input.inputMask
  property alias validator: input.validator

  function focusInput() {
    input.focus = true;
  }

  Text {
    id: label
    text: root.label + ':'
    color: Statics.pallete.foreground
    height: Statics.font_size.middle
    anchors.leftMargin: 10
    anchors.left: root.left
    anchors.verticalCenter: root.verticalCenter
    font.pixelSize: Statics.font_size.normal
  }

  Rectangle {
    id: background
    height: Statics.font_size.large
    anchors{
      verticalCenter: root.verticalCenter
      right: root.right
      rightMargin: 10
      left: label.right
      leftMargin: 5
    }
    color: Statics.pallete.dialogInputBackground
    TextInput {
      id: input
      clip: true
      color: Statics.pallete.foreground
      selectByMouse: true
      anchors{
        left: parent.left
        right: parent.right
        leftMargin: 10
        rightMargin: 10
        verticalCenter: parent.verticalCenter
      }
      font.pixelSize: Statics.font_size.normal
    }
  }
}
