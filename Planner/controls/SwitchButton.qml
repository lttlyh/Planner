import QtQuick 2.0
import Planner 0.1

import '../js/statics.js' as Statics

Item {
  id: root
  property int planstate: Plan.NOT_START

  signal planstateSwipped(var state)

  /* private properties */
  property string larrow_icon: "../resources/larrow.png"
  property string rarrow_icon: "../resources/rarrow.png"
  property string n_start_icon: "../resources/not_started_icon.png"
  property string progressing_icon: "../resources/progressing_icon.png"
  property string finished_icon: "../resources/finished_icon.png"

  function swipeLeft() {
    root.planstate--
    root.planstateSwipped(root.planstate)
//    logState()
  }

  function swipeRight() {
    root.planstate++
    root.planstateSwipped(root.planstate)
//    logState()
  }

  // for debug use
  function logState() {
    console.log("---in switch button----")
    console.log("NOT_START vlaue: " + Plan.NOT_START)
    console.log("PROGRESSING value: " + Plan.PROGRESSING)
    console.log("FINISHED value: " + Plan.FINISHED)
    console.log("PlanState in int: " + root.planstate)
    console.log("Is planstate > NOT_START: " + (root.planstate > Plan.NOT_START).toString())
    console.log("Is planstate < FINISHED: " + (root.planstate < Plan.FINISHED).toString())
    console.log("-----------------------")
  }


  // arrows
  Image {
    width: Statics.iconbutton.imgwidth
    height: Statics.iconbutton.imgheight
    anchors.verticalCenter: parent.verticalCenter
    anchors.right: view.left
    source: root.larrow_icon
    MouseArea {
      anchors.fill: parent
      enabled: root.planstate > Plan.NOT_START
      onClicked: root.swipeLeft()
    }
  }
  Image {
    width: Statics.iconbutton.imgwidth
    height: Statics.iconbutton.imgheight
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: view.right
    source: root.rarrow_icon
    MouseArea {
      anchors.fill: parent
      enabled: root.planstate < Plan.FINISHED
      onClicked: root.swipeRight()
    }
  }

  Item {
    id: view
    clip: true
//    width: Statics.iconbutton.imgwidth
//    height: Statics.iconbutton.imgheight
    width: 40
    height: 40
    anchors.centerIn: parent
    Row {
      id: slidder
      Image {
        width: view.width
        height: view.height
        fillMode: Image.PreserveAspectFit
        verticalAlignment: Image.AlignVCenter
        horizontalAlignment: Image.AlignHCenter
        source: root.n_start_icon
      }
      Image {
        width: view.width
        height: view.height
        fillMode: Image.PreserveAspectFit
        verticalAlignment: Image.AlignVCenter
        horizontalAlignment: Image.AlignHCenter
        source: root.progressing_icon
      }
      Image {
        width: view.width
        height: view.height
        fillMode: Image.PreserveAspectFit
        verticalAlignment: Image.AlignVCenter
        horizontalAlignment: Image.AlignHCenter
        source: root.finished_icon
      }

      Behavior on x {
        NumberAnimation { property: "x"; duration: Statics.time.normal; easing.type: Easing.InOutQuad }
      }
    }
  }

  /* states and transitions */
  states: [
    State {
      name: "NOT_STATED"
      when: root.planstate === Plan.NOT_START
      PropertyChanges {
        target: slidder
        x: 0
      }
    },
    State {
      name: "PROGRESSING"
      when: root.planstate === Plan.PROGRESSING
      PropertyChanges {
        target: slidder
        x: - Statics.iconbutton.imgwidth * 1
      }
    },
    State {
      name: "FINISHED"
      when: root.planstate === Plan.FINISHED
      PropertyChanges {
        target: slidder
        x: - Statics.iconbutton.imgwidth * 2
      }
    }
  ]

  Component.onCompleted: {
//    root.updateState()
//    logState()
  }
}
