import QtQuick 2.0

import '../js/statics.js' as Statics

Item {
  id: root
  //color: Statics.pallete.background
  smooth: true

  signal enter()

  // block out mouse event on main view
  MouseArea {
    anchors.fill: parent
  }

  Text {
    anchors.topMargin: root.height / 4
    anchors.top: root.top
    anchors.horizontalCenter: root.horizontalCenter
    color: Statics.pallete.foreground
    text: "欢迎使用"
    font {
      family: "Courier"
      bold: true
      pixelSize: 36
    }
  }

  Item {
    anchors.bottomMargin: root.height / 3
    anchors.bottom: root.bottom
    anchors.horizontalCenter: root.horizontalCenter
    Text{
      anchors.centerIn: parent
      text:'Start'
      font.pixelSize: 28
      color: Statics.pallete.foreground
      font.bold:true
      MouseArea{
        anchors.fill: parent
        onClicked: root.enter();
      }
    }
  }
}
