import QtQuick 2.0

import '../js/statics.js' as Statics

Item {
  id: root
  width: 200
  height: 150

  property string label
  property alias text: input.text

  Text {
    id: label
    text: root.label + ':'
    color: Statics.pallete.foreground
    height: Statics.font_size.middle
    anchors.leftMargin: 10
    anchors.left: root.left
    anchors.top: root.top
    font.pixelSize: Statics.font_size.normal
  }

  Rectangle {
    id: background
    anchors{
      right: root.right
      left: root.left
      bottom: root.bottom
      top: label.bottom
      margins: 10
    }
    color: Statics.pallete.dialogInputBackground
    Flickable {
      id: flick
      contentWidth: input.paintedWidth
      contentHeight: input.paintedHeight
      clip: true
      anchors{
        fill: parent
        margins: 10
      }

      function ensureVisible(r)
      {
        if (contentX >= r.x)
          contentX = r.x;
        else if (contentX+width <= r.x+r.width)
          contentX = r.x+r.width-width;
        if (contentY >= r.y)
          contentY = r.y;
        else if (contentY+height <= r.y+r.height)
          contentY = r.y+r.height-height;
      }

      TextEdit {
        id: input
        width: flick.width
        height: flick.height
        focus: true
        wrapMode: TextEdit.Wrap
        onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
        color: Statics.pallete.foreground
        selectByMouse: true
        font.pixelSize: Statics.font_size.normal
      }
    }
  }
}
