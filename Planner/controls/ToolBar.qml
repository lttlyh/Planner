import QtQuick 2.0

import '../js/statics.js' as Statics

Rectangle {
  id: root
  color: Statics.pallete.control_color

  signal addClicked()

  // bottom border
  Rectangle {
    width: parent.width
    height: 4
    color: Qt.darker(root.color)
    anchors.bottom: parent.bottom
  }

  Row {
    id: row
    anchors.centerIn: parent
    spacing: Statics.toolbar.spacing

    Image {
      source: "../resources/file_add.png"
      sourceSize.height: root.height * 2 / 3
    }

    Text {
      text: "添加安排"
      color: Statics.pallete.foreground
      anchors.verticalCenter: row.verticalCenter
      font {
        pixelSize: Statics.font_size.middle
      }
    }
  }

  MouseArea {
    anchors.fill: row
    onClicked: root.addClicked()
  }

  // close Button
  Text {
    text: "X"
    font.pixelSize: Statics.font_size.normal
    color: xbtnma.containsMouse ? Qt.lighter(Statics.pallete.critical) : Statics.pallete.critical
    anchors.margins: 10
    anchors.top: parent.top
    anchors.right: parent.right

    Behavior on color {
      ColorAnimation { duration: Statics.time.normal }
    }

    MouseArea {
      id: xbtnma
      anchors.fill: parent
      hoverEnabled: true
      onClicked: Qt.quit()
    }
  }
}
