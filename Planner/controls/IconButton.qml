import QtQuick 2.0

import '../js/statics.js' as Statics

Image {
  id: root
  width: Statics.iconbutton.imgwidth
  height: Statics.iconbutton.imgheight
  horizontalAlignment: Image.AlignHCenter
  verticalAlignment: Image.AlignVCenter
  fillMode: Image.PreserveAspectFit

  signal clicked()

  MouseArea {
    anchors.fill: parent
    onClicked: root.clicked()
  }
}
