import QtQuick 2.0
import Planner 0.1

import '../js/statics.js' as Statics
import '../js/utils.js' as Utils

Item {
  id: root
  clip: true

  property int idx
  property var datamodel
  property bool isCurrentItem
  property string title: datamodel.title
  property date date: datamodel.date
  property string description: datamodel.description
  property int planstate: datamodel.state

  signal clicked
  signal delClicked
  signal editClicked
  signal planstateSwipped(int state)

  property bool showDetailed: true
  width: 300
  height: 80

  function planstateSwippedHandler(state) {
    root.planstateSwipped(state);
  }

  // helper function for date strring generation
  function dateString(d){
    var arr = [];
    arr.push('<b>');
    arr.push(Utils.truncateLeft(d.getFullYear(), 4));
    arr.push('-');
    arr.push(Utils.truncateLeft(d.getMonth() + 1, 2));
    arr.push('-');
    arr.push(Utils.truncateLeft(d.getDate() , 2));
    arr.push('</b><br/>');
    arr.push('<small>');
    arr.push(Utils.truncateLeft(d.getHours(), 2));
    arr.push(':');
    arr.push(Utils.truncateLeft(d.getMinutes(), 2));
    arr.push('</small>');
    return arr.join("");
  }

  Rectangle {
    id: container
    anchors.left: root.left
    anchors.right: buttons.left
    height: root.height
    color: Statics.pallete.control_color
    /* contents */
    // bottom border
    Rectangle {
      height: 1
      color: Qt.darker(parent.color)
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: parent.right
    }

    Text {
      id: lb_title
      anchors.top: container.top
      anchors.left: container.left
      anchors.right: lb_date.left
      anchors.margins: 4
      text: root.title
      color: Statics.pallete.foreground
      elide: Text.ElideRight
      font.pixelSize: Statics.font_size.middle
      font.bold: true
    }
    Text {
      id: lb_date
      anchors.top: container.top
      anchors.right: container.right
      anchors.margins: 4
      text: dateString(root.date)
      color: Statics.pallete.foreground
      font.pixelSize: Statics.font_size.normal
    }
    Text {
      id: lb_desc
      anchors.top: container.top
      anchors.bottom: container.bottom
      anchors.left: container.left
      anchors.right: container.right
      anchors.margins: 4
      anchors.topMargin: Statics.planviewitem.height / 2
      text: root.description
      color: Statics.pallete.foreground
      elide: Text.ElideRight
      wrapMode: Text.Wrap
      font.pixelSize: Statics.font_size.normal
    }

    /* globalma */
    MouseArea{
      id: rootma
      anchors.fill: container
      onClicked: {
        root.clicked();
      }
    }
  }

  /* function buttons */
  Item {
    id: buttons
    clip: true
    width: 0
    height: root.height
    anchors.right: root.right

    // left border
    Rectangle {
      width: 4
      height: parent.height
      anchors.left: parent.left
      color: Qt.darker(Statics.pallete.background)
    }

    Column {
      id: fBtnsCol
      anchors.fill: parent
      // three icon buttons here (delete, modify, state switch)
      Item {
        width: fBtnsCol.width
        height: fBtnsCol.height / 3
        IconButton {
          id: delBtn
          source: "../resources/del_icon.png"
          anchors.centerIn: parent
          onClicked: root.delClicked()
        }
      }
      Item {
        width: fBtnsCol.width
        height: fBtnsCol.height / 3
         IconButton {
          id: modifyBtn
          source: "../resources/edit_icon.png"
          anchors.centerIn: parent
          onClicked: root.editClicked()
        }
      }
      Item {
        width: fBtnsCol.width
        height: fBtnsCol.height / 3
        SwitchButton {
          anchors.centerIn: parent
          onPlanstateSwipped: root.planstateSwippedHandler(state)
          planstate: root.planstate
        }
       }
    }
  }

  /* states and transitions */
  states: [
    State {
      name: "detailed"
      when: root.showDetailed
      PropertyChanges {
        target: buttons
        width: Statics.planviewitem.sidebarwidth
      }

      PropertyChanges {
        target: root
        height: Statics.planviewitem.height * 2
      }
    },
    State {
      name: "initial"
      PropertyChanges {
        target: root
        x: root.width
      }
    }
  ]

  transitions: [
     Transition {
      from: ""
      to: "detailed"
      reversible: true
      NumberAnimation { target: buttons; property: "width"; duration: Statics.time.normal; easing.type: Easing.InOutCubic }
      NumberAnimation { target: root; property: "height"; duration: Statics.time.normal; easing.type: Easing.InOutCubic }
    },
    Transition {
      from: "initial"
      to: ""
      NumberAnimation { target: root; property: "x"; duration: Statics.time.normal; easing.type: Easing.InOutCubic }
    }
  ]

  Component.onCompleted: {
    root.state = "initial"
    root.state = ""
  }
}
