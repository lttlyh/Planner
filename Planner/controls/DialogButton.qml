import QtQuick 2.0

import '../js/statics.js' as Statics

Rectangle {
  id: cancelBtn
  property string text: 'button'
  property color btnColor: 'green'
  color: btnma.pressed ? Qt.darker(btnColor) : btnma.containsMouse ? Qt.lighter(btnColor) : btnColor
  signal clicked()
  Behavior on color {
    ColorAnimation { duration: Statics.time.quick }
  }
  Text {
    text: parent.text
    anchors.centerIn: parent
    color: Statics.pallete.foreground
    font.pixelSize: Statics.font_size.normal
    font.bold: true
  }
  MouseArea {
    id: btnma
    anchors.fill: parent
    hoverEnabled: true
    onClicked: parent.clicked()
  }
}
