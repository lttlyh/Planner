#ifndef PLANNERMODEL_H
#define PLANNERMODEL_H

#include <QAbstractListModel>
#include "plan.h"

class PlannerModel : public QAbstractListModel
{
  Q_OBJECT
  Q_ENUMS(PLANNER_ROLE)
public:
  enum PLANNER_ROLE {
    TITLE = Qt::UserRole + 1,
    DATE,
    DESCRIPTION,
    STATE,
    STATE_PRINTABLE
  };

  static const char *TITLE_STR;
  static const char *DATE_STR;
  static const char *DESCRIPTION_STR;
  static const char *STATE_STR;
  static const char *STATE_PRINTABLE_STR;

  explicit PlannerModel(QObject *parent = 0);
  virtual int rowCount(const QModelIndex &parent) const;
  virtual QVariant data(const QModelIndex &index, int role) const;
  virtual Qt::ItemFlags flags(const QModelIndex &index) const;
  virtual QHash<int, QByteArray> roleNames() const;
  virtual bool insertRows(int row, int count, const QModelIndex &parent);
  virtual bool removeRows(int row, int count, const QModelIndex &parent);
  virtual bool moveRows(const QModelIndex &, int, int, const QModelIndex &, int);

  void import(QList<Plan *> plans);
  Q_INVOKABLE void add(Plan *p);
  Q_INVOKABLE void remove(int i);
  Q_INVOKABLE void modify(int i, const QVariant &value, int role);
  Q_INVOKABLE void beginUpdate();
  Q_INVOKABLE void endUpdate();

signals:

public slots:

private:
  static QList<Plan *> m_plans;
  static QObject gparent;
  static QHash<int, QVector<int> > changes;
};

#endif // PLANNERMODEL_H
