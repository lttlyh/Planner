#include "plan.h"

const char *Plan::NOT_START_STR = "Not Started";
const char *Plan::PROGRESSING_STR = "Progressing";
const char *Plan::FINISHED_STR = "Finished";

Plan::Plan(QObject *parent) :
  QObject(parent),
  m_id(-1)
{
}

Plan::Plan(const Plan &p, QObject *parent) :
  QObject(parent),
  m_title(p.title()),
  m_date(p.date()),
  m_description(p.description()),
  m_state(p.state()),
  m_id(-1)
{
}

QString Plan::title() const
{
  return m_title;
}

void Plan::setTitle(const QString &title)
{
  if(title == m_title)
    return;
  m_title = title;
  emit titleChanged();
}

QDateTime Plan::date() const
{
  return m_date;
}

void Plan::setDate(const QDateTime &date)
{
  if(date == m_date)
    return;
  m_date = date;
  emit dateChanged();
}

QString Plan::description() const
{
  return m_description;
}

void Plan::setDescription(const QString &desc)
{
  if(desc == m_description)
    return;
  m_description = desc;
  emit descriptionChanged();
}

Plan::PLAN_STATE Plan::state() const
{
  return m_state;
}

void Plan::setState(int s)
{
  if(s == m_state)
    return;
  m_state = (Plan::PLAN_STATE)s;
  emit stateChanged();
}

int Plan::id() const
{
  return m_id;
}

void Plan::setId(int i)
{
  m_id = i;
}

bool Plan::operator <(const Plan &another) const
{
  if(m_state > another.m_state) // a plan is less important with greter state value (consult the definition of state enum)
    return true;
  else if(m_state < another.m_state)
    return false;
  else
    return m_date < another.m_date;
}
