TEMPLATE = app

QT += qml quick sql widgets

SOURCES += main.cpp \
    plannermodel.cpp \
    plan.cpp \
    persistentstorage.cpp

RESOURCES += planner.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    plannermodel.h \
    plan.h \
    persistentstorage.h
